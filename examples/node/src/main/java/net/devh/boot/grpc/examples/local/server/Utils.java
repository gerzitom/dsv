package net.devh.boot.grpc.examples.local.server;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.ManagedChannelProvider;
import io.grpc.ManagedChannelRegistry;
import net.devh.boot.grpc.examples.lib.Address;
import net.devh.boot.grpc.examples.lib.SimpleGrpc;

public class Utils {
    public static String formatAddress(Address address) {
        return new StringBuilder()
                .append("[")
                .append(address.getIp())
                .append(", ")
                .append(address.getPort())
                .append("]")
                .toString();
    }

    public static ManagedChannel getChannel(Address address) {
        return ManagedChannelBuilder.forAddress(address.getIp(), address.getPort())
                .usePlaintext()
                .build();
    }

    public static SimpleGrpc.SimpleBlockingStub createStubFromAddress(Address address){
        final ManagedChannel channel = getChannel(address);
        return SimpleGrpc.newBlockingStub(channel);
    }

    public static SimpleGrpc.SimpleBlockingStub createStubFromAddress(ManagedChannel channel){
        return SimpleGrpc.newBlockingStub(channel);
    }
}