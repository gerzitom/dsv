package net.devh.boot.grpc.examples.local.server;

import lombok.Getter;
import lombok.Setter;
import net.devh.boot.grpc.examples.lib.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.net.InetAddress;

@Service
@Getter
@Setter
@PropertySource("classpath:application-${spring.profiles.active}.yaml")
public class NodeData {
    private String name;
    private String variable;
    private Address address;
    private Address next;
    private Address prev;
    private Address leader;
    private long id;
    private boolean voting = false;
    private boolean changingValue = false;

    public NodeData(
            ApplicationArguments applicationArguments,
            @Value("${config.name}") String name,
            @Value("${grpc.server.port}") int port
    ) {
        String hostname = determineHostName();
        this.name = name;
        Address myAddress = Address.newBuilder().setIp(hostname).setPort(port).build();
        this.address = myAddress;
        this.leader = myAddress;
        this.next = myAddress;
        this.id = generateId(hostname, port);
        if(applicationArguments.getNonOptionArgs().size() == 2){
            this.buildOtherAddress(applicationArguments);
        } else {
            this.prev = myAddress;
            this.variable = applicationArguments.getNonOptionArgs().get(0);
        }
    }

    public boolean isAlone(){
        return next.equals(prev) && next.equals(address);
    }

    private String determineHostName(){
        try{
            return InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e){
            return "127.0.0.1";
        }
    }

    private void buildOtherAddress(ApplicationArguments applicationArguments) {
        String otherHostname = applicationArguments.getNonOptionArgs().get(0);
        if(otherHostname.equals("localhost")) otherHostname = determineHostName();
        int otherPort = Integer.parseInt(applicationArguments.getNonOptionArgs().get(1));
        this.prev = Address.newBuilder().setIp(otherHostname).setPort(otherPort).build();
    }

    private long generateId(String address, int port) {
        // generates  <port><IPv4_dec1><IPv4_dec2><IPv4_dec3><IPv4_dec4>
        String[] array = address.split("\\.");
        long id = 0;
        long shift = 0, temp = 0;
        for(int i = 0 ; i < array.length; i++){
            temp = Long.parseLong(array[i]);
            id = (long) (id * 1000);
            id += temp;
        }
        if (id == 0) {
            // TODO problem with parsing address - handle it
            id = 666000666000l;
        }
        id = id + port*1000000000000l;
        return id;
    }
}
