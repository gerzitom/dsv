/*
 * Copyright (c) 2016-2021 Michael Zhang <yidongnan@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.devh.boot.grpc.examples.local.server;

import io.grpc.ManagedChannel;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.examples.lib.Address;
import net.devh.boot.grpc.examples.lib.ElectedMessage;
import net.devh.boot.grpc.examples.lib.ElectionMessage;
import net.devh.boot.grpc.examples.lib.EmptyMessage;
import net.devh.boot.grpc.examples.lib.HelloReply;
import net.devh.boot.grpc.examples.lib.HelloRequest;
import net.devh.boot.grpc.examples.lib.JoinResponse;
import net.devh.boot.grpc.examples.lib.NewValueMessage;
import net.devh.boot.grpc.examples.lib.NewValueResponse;
import net.devh.boot.grpc.examples.lib.SimpleGrpc;
import net.devh.boot.grpc.examples.lib.ValueResponse;
import net.devh.boot.grpc.examples.local.server.cli.ShellHelper;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.rmi.RemoteException;

import static net.devh.boot.grpc.examples.local.server.Utils.createStubFromAddress;

/**
 * @author Michael (yidongnan@gmail.com)
 * @since 2016/11/8
 */

@GrpcService
@Slf4j
public class GrpcServerService extends SimpleGrpc.SimpleImplBase {

    @Autowired
    private NodeData nodeData;

    @Autowired
    private ShellHelper shellHelper;

    private boolean repairInProgress = false;
    private boolean checkingTopology = false;

    @Autowired
    private Connections connections;

    @Override
    public void sayHello(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
        HelloReply reply = HelloReply.newBuilder().setMessage("Hello ==> " + req.getName()).build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }

    @Override
    public void join(Address request, StreamObserver<JoinResponse> responseObserver) {
        shellHelper.printInfo("Someone is joining with address" + Utils.formatAddress(request));

        Address next = nodeData.getNext();
        Connections.shutdownChannel(connections.getNext());
        nodeData.setNext(request);
        connections.setNext(Connections.buildConnection(request));

        JoinResponse joinResponse = JoinResponse.newBuilder()
                .setLeader(nodeData.getLeader())
                .setVariable(nodeData.getVariable())
                .setNext(next)
                .build();
        responseObserver.onNext(joinResponse);
        responseObserver.onCompleted();
    }

    @Override
    public void changePrev(Address newPrev, StreamObserver<Address> responseObserver) {
        shellHelper.printInfo(nodeData.getName() + " has changed prev node to " + Utils.formatAddress(newPrev));
        Connections.shutdownChannel(connections.getPrev());
        connections.setPrev(Connections.buildConnection(newPrev));
        nodeData.setPrev(newPrev);
        responseObserver.onNext(nodeData.getAddress());
        responseObserver.onCompleted();
    }

    @Override
    public void changeNext(Address newNext, StreamObserver<Address> responseObserver) {
        shellHelper.printInfo(nodeData.getName() + " has changed next node to " + Utils.formatAddress(newNext));
        Connections.shutdownChannel(connections.getNext());
        connections.setNext(Connections.buildConnection(newNext));
        nodeData.setNext(newNext);
        responseObserver.onNext(nodeData.getAddress());
        responseObserver.onCompleted();
    }

    @Override
    public void nodeMissing(Address missingNode, StreamObserver<Address> responseObserver) {
        // is it for me
        if(missingNode.equals(nodeData.getPrev())){
            shellHelper.printInfo("Missing node is my previous node.");
            // close connection with missing node
            Connections.shutdownChannel(connections.getPrev());
            responseObserver.onNext(nodeData.getAddress());
            responseObserver.onCompleted();
        } else {
            shellHelper.printInfo("Missing node is not my previous node. So I am sending request to my previous node.");
            SimpleGrpc.SimpleBlockingStub prevNode = connections.getPrevNode();
            Address newAddress = prevNode.nodeMissing(missingNode);
            responseObserver.onNext(newAddress);
            responseObserver.onCompleted();
        }
    }

    @Override
    public void newValue(NewValueMessage request, StreamObserver<NewValueResponse> responseObserver) {
        shellHelper.printInfo("New request for changing value reveived from " + Utils.formatAddress(request.getFrom()) + " to update value to: " + request.getVariable());
        nodeData.setVariable(request.getVariable());
        responseObserver.onNext(NewValueResponse.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void readValue(EmptyMessage request, StreamObserver<ValueResponse> responseObserver) {
        shellHelper.printInfo("New request for reading value received.");
        responseObserver.onNext(ValueResponse.newBuilder().setValue(nodeData.getVariable()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void setValue(NewValueMessage request, StreamObserver<EmptyMessage> responseObserver) {
        nodeData.setVariable(request.getVariable());
        shellHelper.printInfo("Value was set from " + Utils.formatAddress(request.getFrom()) + " to " + request.getVariable());
        responseObserver.onNext(EmptyMessage.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void checkTopology(Address request, StreamObserver<EmptyMessage> responseObserver) {
        shellHelper.printInfo("Topology check requested from " + Utils.formatAddress(request) + ", just checking if you are still there.");
        if(!nodeData.isAlone()){
            try {
                if(request.equals(nodeData.getAddress())){
                    shellHelper.printSuccess("Topology check ended.");
                } else {
                    SimpleGrpc.SimpleBlockingStub nextNode = connections.getNextNode();
                    nextNode.checkTopology(request);
                }
            } catch (Exception e) {
                repairTolopogy();
            }
        } else {
            shellHelper.printInfo("I am only one in the system, so no topology check is needed.");
        }
        responseObserver.onNext(EmptyMessage.newBuilder().build());
        responseObserver.onCompleted();
    }


    @Override
    public void election(ElectionMessage request, StreamObserver<EmptyMessage> responseObserver) {
        long id = request.getId();
        shellHelper.printInfo("Election was called with id " + id);
        if (nodeData.getId() < id) {
            nodeData.setVoting(true);
            SimpleGrpc.SimpleBlockingStub nextNode = connections.getNextNode();
            nextNode.election(ElectionMessage.newBuilder().setId(id).build());
        } else if ((nodeData.getId() > id) & (nodeData.isVoting() == false)) {
            nodeData.setVoting(true);
            SimpleGrpc.SimpleBlockingStub nextNode = connections.getNextNode();
            nextNode.election(ElectionMessage.newBuilder().setId(nodeData.getId()).setVariable(request.getVariable()).build());
        } else if (nodeData.getId() == id) {
            // same -> I am leader
            SimpleGrpc.SimpleBlockingStub nextNode = connections.getNextNode();
            nodeData.setVariable(request.getVariable());
            nextNode.elected(ElectedMessage.newBuilder().setAddress(nodeData.getAddress()).setId(id).build());
        }
        responseObserver.onNext(EmptyMessage.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void elected(ElectedMessage request, StreamObserver<EmptyMessage> responseObserver) {
        long id = request.getId();
        Address leaderAddr = request.getAddress();
        shellHelper.printSuccess("Elected was called with id " + id + " and with address " + Utils.formatAddress(leaderAddr));
        shellHelper.printWarning("Be aware, that variable might be missing, because previous leader is dead. Try reading value.");
        connections.getLeader().shutdownNow();
        connections.setLeader(Connections.buildConnection(leaderAddr));
        nodeData.setLeader(leaderAddr);
        if (nodeData.getId() != id) {
            SimpleGrpc.SimpleBlockingStub nextNode = connections.getNextNode();
            nextNode.elected(ElectedMessage.newBuilder().setAddress(leaderAddr).setId(id).build());
        }
        responseObserver.onNext(EmptyMessage.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void repairTopology(NewValueMessage request, StreamObserver<EmptyMessage> responseObserver) {
        repairTolopogy();
        responseObserver.onNext(EmptyMessage.newBuilder().build());
        responseObserver.onCompleted();
    }

    public void repairTolopogy() {
        if (!repairInProgress) {
            repairInProgress = true;
            Address failedNode = nodeData.getNext();
            shellHelper.printError("Topology was broken.");
            shellHelper.printError("Address " + Utils.formatAddress(failedNode) + " can not be found.");
            shellHelper.printInfo("Looking for other broken node.");

            // Am I alone?
            if(failedNode.equals(nodeData.getPrev())) {
                shellHelper.printInfo("It seems I am only one in this system.");
                nodeData.setNext(nodeData.getAddress());
            } else {
                Address newNext = connections.getPrevNode().nodeMissing(nodeData.getNext());
                shellHelper.printSuccess("Broken node found with address " + Utils.formatAddress(newNext) + " not responding.");
                connections.getNext().shutdownNow();
                connections.setNext(Connections.buildConnection(newNext));
                nodeData.setNext(newNext);
                SimpleGrpc.SimpleBlockingStub newNextNode = connections.getNextNode();
                newNextNode.changePrev(nodeData.getAddress());
                // it was leader
                if(failedNode.equals(nodeData.getLeader())){
                    // start election
                    SimpleGrpc.SimpleBlockingStub me = connections.getMeNode();
                    me.election(ElectionMessage.newBuilder().setId(nodeData.getId()).build());
                }
            }
            repairInProgress = false;
        }
    }


}
