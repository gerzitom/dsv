/*
 * Copyright (c) 2016-2021 Michael Zhang <yidongnan@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.devh.boot.grpc.examples.local.server;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.channelz.v1.Channel;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.examples.lib.Address;
import net.devh.boot.grpc.examples.lib.ElectionMessage;
import net.devh.boot.grpc.examples.lib.EmptyMessage;
import net.devh.boot.grpc.examples.lib.HelloReply;
import net.devh.boot.grpc.examples.lib.HelloRequest;
import net.devh.boot.grpc.examples.lib.JoinResponse;
import net.devh.boot.grpc.examples.lib.NewValueMessage;
import net.devh.boot.grpc.examples.lib.NewValueResponse;
import net.devh.boot.grpc.examples.lib.SimpleGrpc;
import net.devh.boot.grpc.examples.lib.SimpleGrpc.SimpleBlockingStub;
import net.devh.boot.grpc.examples.lib.ValueResponse;
import net.devh.boot.grpc.examples.local.server.cli.ShellHelper;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.rmi.RemoteException;
import java.util.concurrent.TimeUnit;

import static net.devh.boot.grpc.examples.local.server.Utils.createStubFromAddress;
import static net.devh.boot.grpc.examples.local.server.Utils.getChannel;

/**
 * @author Michael (yidongnan@gmail.com)
 * @since 2016/11/8
 */
@Service
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GrpcClientService {

    @Autowired
    private NodeData nodeData;

    @Autowired
    private ShellHelper shellHelper;

    private boolean repairInProgress = false;

    @Autowired
    public Connections connections;

    @EventListener(ApplicationStartedEvent.class)
    public void doSomethingAfterStartup() {
        shellHelper.printInfo(nodeData.getName() + " is joining to the system with address: " + Utils.formatAddress(nodeData.getAddress()));
        Address myAdress = nodeData.getAddress();
        Address myPrev = nodeData.getPrev();
        boolean startupNode = myAdress.equals(myPrev);
        Address myAddress = nodeData.getAddress();
        try{
            connections.setMe(Connections.buildConnection(nodeData.getAddress()));
            connections.setLeader(Connections.buildConnection(myAddress));
        } catch (Exception e) {
            shellHelper.printError(e.getMessage());
        }
        if(!startupNode){
            shellHelper.printInfo(nodeData.getName() + " will be joining on existing node" + Utils.formatAddress(myPrev));
            try{
                // build prev connection
                connections.setPrev(Connections.buildConnection(myPrev));
                SimpleBlockingStub prevNode = connections.getPrevNode();

                JoinResponse joinResponse = prevNode.join(nodeData.getAddress());
                Address newNext = joinResponse.getNext();

                connections.setNext(Connections.buildConnection(newNext));
                SimpleBlockingStub nextNode = connections.getNextNode();

                nextNode.changePrev(nodeData.getAddress());
                nodeData.setNext(newNext);
                nodeData.setVariable(joinResponse.getVariable());

                // make leader connection
                connections.setLeader(Connections.buildConnection(joinResponse.getLeader()));
                nodeData.setLeader(joinResponse.getLeader());
            } catch (Exception e) {
                shellHelper.printError(e.getMessage());
            }
        } else {
            shellHelper.printInfo(nodeData.getName() + " is the first node");
        }
        shellHelper.printSuccess("Connected to system");
    }

    public void leave() {
        shellHelper.printInfo("Leaving the system. Setting up leaving steps");
        // tell my prev node to update its next
        SimpleBlockingStub prevNode = connections.getPrevNode();
        prevNode.changeNext(nodeData.getNext());

        // tell my next node to update its prev
        SimpleBlockingStub nextNode = connections.getNextNode();
        nextNode.changePrev(nodeData.getPrev());

        // if I am leader and leaving start election on my next node
        if(nodeData.getAddress().equals(nodeData.getLeader())){
            nextNode.election(ElectionMessage.newBuilder().setId(-1).setVariable(nodeData.getVariable()).build());
        }

        connections.shutdownAll();
        shellHelper.printSuccess("Leaving done. Bye");
    }

    public void testElection(){
        SimpleBlockingStub me = connections.getMeNode();
        me.election(ElectionMessage.newBuilder().setId(-1).build());
    }

    public String readValue(){
        SimpleBlockingStub leaderAddress = connections.getLeaderNode();
        ValueResponse response = leaderAddress.readValue(EmptyMessage.newBuilder().build());
        return response.getValue();
    }

    public void writeValue(String value){
        boolean cont = true;
        while (cont){
            try {
                shellHelper.printInfo("Sending request to write new value: " + value + ".");
                SimpleBlockingStub leaderAddress = connections.getLeaderNode();
                leaderAddress.newValue(NewValueMessage.newBuilder().setVariable(value).build());
                shellHelper.printSuccess("Value was changed to " + value);
                cont = false;
            } catch (Exception e) {
                shellHelper.printError("Error during writing value.");
                connections.getMeNode().repairTopology(NewValueMessage.newBuilder().build());
                shellHelper.printInfo("Try to repeat writing variable.");
            }
        }
    }

    public void checkTopology(){
        try {
            SimpleBlockingStub myNext = connections.getNextNode();
            myNext.checkTopology(nodeData.getAddress());
        } catch (Exception e) {
//            connections.getLeaderNode()
            connections.getMeNode().repairTopology(NewValueMessage.newBuilder().build());
        }
    }

    public void helloPrev(){
        HelloReply reply = connections.getPrevNode().sayHello(HelloRequest.newBuilder().setName("Hello").build());
        shellHelper.printInfo(reply.getMessage());
    }
}
