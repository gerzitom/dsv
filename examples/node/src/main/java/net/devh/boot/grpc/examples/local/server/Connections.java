package net.devh.boot.grpc.examples.local.server;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.Getter;
import lombok.Setter;
import net.devh.boot.grpc.examples.lib.Address;
import net.devh.boot.grpc.examples.lib.SimpleGrpc;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Getter
@Setter
@Component
public class Connections {
    private ManagedChannel next;
    private ManagedChannel prev;
    private ManagedChannel leader;
    private ManagedChannel me;

    public SimpleGrpc.SimpleBlockingStub getNextNode(){
        return SimpleGrpc.newBlockingStub(this.next);
    }

    public SimpleGrpc.SimpleBlockingStub getPrevNode(){
        return SimpleGrpc.newBlockingStub(this.prev);
    }

    public SimpleGrpc.SimpleBlockingStub getMeNode(){
        return SimpleGrpc.newBlockingStub(this.me);
    }

    public SimpleGrpc.SimpleBlockingStub getLeaderNode(){
        return SimpleGrpc.newBlockingStub(this.leader);
    }

    public void shutdownAll(){
        if(this.next != null) this.next.shutdownNow();
        if(this.prev != null) this.prev.shutdownNow();
        if(this.me != null) this.me.shutdownNow();
        if(this.leader != null) this.leader.shutdownNow();

        try {
            if(this.next != null) this.next.awaitTermination(5000, TimeUnit.MILLISECONDS);
            if(this.prev != null) this.prev.awaitTermination(5000, TimeUnit.MILLISECONDS);
            if(this.me != null) this.me.awaitTermination(5000, TimeUnit.MILLISECONDS);
            if(this.leader != null) this.leader.awaitTermination(5000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void shutdownChannel(ManagedChannel channel){
        if(channel != null){
            channel.shutdownNow();
            try{
                channel.awaitTermination(5000, TimeUnit.MILLISECONDS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static ManagedChannel buildConnection(Address address){
        return ManagedChannelBuilder.forAddress(address.getIp(), address.getPort())
                .usePlaintext()
                .build();
    }
}
