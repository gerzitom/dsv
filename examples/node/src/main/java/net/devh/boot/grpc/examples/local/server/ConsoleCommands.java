package net.devh.boot.grpc.examples.local.server;

import net.devh.boot.grpc.examples.local.server.cli.ShellHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class ConsoleCommands {

    @Autowired
    NodeData nodeData;

    @Autowired
    ShellHelper shellHelper;

    @Autowired
    GrpcClientService clientService;

    @ShellMethod("Read value")
    public void value() {
        String value = clientService.readValue();
        if(value.equals("")){
            shellHelper.printError("Value was not found on the system, it might not been initialized yet." +
                    "\n If you want to set new value, please add it via command `write` for example: `write new_value`");
        } else {
            shellHelper.printSuccess("Value of the system is " + value);
        }
    }

    @ShellMethod("write wariable")
    public void write(String variable) {
        clientService.writeValue(variable);
    }

    @ShellMethod("Check ring topology")
    public void checkTopology(){
        clientService.checkTopology();
    }

    @ShellMethod("Check ring topology")
    public void ct(){
        clientService.checkTopology();
    }

    @ShellMethod("Show config")
    public void config(){
        shellHelper.printInfo("Printed config" +
                "\n-------------------------------------" +
                "\nCONFIG:" +
                "\nMy channel: " + clientService.connections.getMe().authority() +
                "\nMy id: " + nodeData.getId() +
                "\nMy address: " + Utils.formatAddress(nodeData.getAddress()) +
                "\nMy next: " + Utils.formatAddress(nodeData.getNext()) +
                "\nMy prev: " + Utils.formatAddress(nodeData.getPrev()) +
                "\nMy leader: " + Utils.formatAddress(nodeData.getLeader()) +
                "\n-------------------------------------"
                );
    }


    @ShellMethod("Leave system")
    public void leave(){
        clientService.leave();
        System.exit(0);
    }


    @ShellMethod("Send hello to prev node")
    public void helloPrev(){
        clientService.helloPrev();
    }

    @ShellMethod("Starts election")
    public void election(){
        clientService.testElection();
    }
}
