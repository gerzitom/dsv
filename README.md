# gRPC Spring Boot Starter
Toto je moje zpráva o projektu z předmětu DSV.

Pro práci s gRPC jsem použil starter kit pro [Spring](https://yidongnan.github.io/grpc-spring-boot-starter/en/).
Samotný projekt je uložen ve složce examples a je to projekt node.
Ještě je zde projekt grpc-lib, ve které je .proto soubor pro specifikaci protokolu, kterým se nody baví a slouží jako generátor grpc souborů pro javu

## Jak spustit
Aplikace se pouší puštěním aplikace Node s profilem, který si zvolíte, defaultně je zde připravených 5 profilů (node1-5). Tyto profily mají každý nastavené defaultní hodnoty v .yaml filech.
Poté se každá noda musí ještě nastavit v parametrech programu a to: 
- pokud chci mít nodu jako první, použiji je jeden parametr a to defaultní proměnnou.
- pokud se noda připojuje k systému, tak specifikujte 2 parametry a to adresu stroje a port.
  Pokud chcete nodu připojit na tento stroj, použijte klíčové slovo `localhost`, takže například `localhost 9890`.
  Program si určuje ip adresu sám a tímto klíčovým slovem mu řeknete, aby si ji vyhledal v lokálním stroji.

## Architektura aplikace
Každá noda si pamatuje své sousedy (next, prev), potom leadera a proměnnou, pokud je leader.

Obnovu topologie můžeme vyvolat ručně příkazem `check-topology` a implementovaný algoritmus se spustí při obnově topologie.
Aplikace je rozdělená na tyto servisy:
### ConsoleCommands
Tato service se stará o správu konzole, jsou zde nadefinované všechny možné příkazy pro aplikaci.

- `value` - získá proměnnou ze systému 
- `write` - zapíše proměnnou do systému  
- `check-topology` nebo `ct` - projde celý systém a zjistí, jestli všechny nody fungují, jestli ne, pokusí se opravit topologii.  
- `config` - ukáže nastavení aktuálního nodu, jakou má adresu a jaké má sousedy  
- `leave` - bezpečně odpojí nodu ze systému

### NodeData
Drží informace o dané nodě, její sousedy, proměnnou (pokud je leader) a taky leadera.

### Connections
Komponenta, která drží jednotlivé connections. V gRPC je totiž problém, že pokud se nějaký cahnnel nezavře, tak se mu to úplně nelíbí.
Taky vytváření kanálů je dost náročné, proto se tady v této komponentě vše drží.

### GrpcClientService
Jakkdyby klientská část nodu, zpravidla jsou zde implementované metody, která se volají z konzole a tyto metody volají servery jiných nodů.
Pake je tady ještě inicializační metoda, která nastaví node při zapnutí procesu a připojí ho do systému.

### GrpcServerService
Servrová část nodu. Tady se děje hlavní magie, zpracovávají se tady požadavky jiných nodů a pak také volají další nody.

